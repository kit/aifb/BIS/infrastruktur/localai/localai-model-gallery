## LocalAI Readme

**Overview**

LocalAI is a platform designed to leverage the power of large language models (LLMs) for local use. To adapt to the rapid evolution of models, LocalAI uses aliases in its code to refer to specific models. This ensures that model updates do not disrupt functionality.

**Model Aliases**

| Alias                      | Description                                                                                                                               | Current Model (May 2024)                                                                                 |
| -------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------- |
| `alias-code`               | Model specifically trained for code generation tasks.                                                                                       | [Starcoder2-15B](https://huggingface.co/bigcode/starcoder)                                                   |
| `alias-embeddings`         | Model optimized for generating text embeddings.                                                                                            | [all-MiniLM-L6-v2](https://huggingface.co/sentence-transformers/all-MiniLM-L6-v2)                              |
| `alias-embeddings-large`   | Larger model designed for generating text embeddings, potentially offering higher accuracy but with increased resource consumption.         | [GritLM-7B](https://huggingface.co/GritLM/GritLM-7B)                                                         |
| `alias-fast`               | Model prioritized for high-throughput processing, suitable for tasks requiring rapid responses.                                          | [Neural](https://huggingface.co/Intel/neural-chat-7b-v3-3)                                                 |
| `alias-fast-german`       | Model prioritized for high-throughput processing, suitable for tasks requiring rapid responses. Can speak german.                           | [SauerkrautLM 7B HerO](https://huggingface.co/TheBloke/sauerkrautLM-7B-HerO-GGUF)                                                 |
| `alias-fast-instruct`      | Instruction-fine-tuned model designed for high-throughput processing, ideal for tasks involving instructions or prompts.                   | [Llama 3 Instruct](https://huggingface.co/meta-llama/Meta-Llama-3-8B-Instruct)                             |
| `alias-large`              | The largest available model, offering the highest potential accuracy but with slower performance compared to smaller models.                | [Mixtral 8x7B](https://huggingface.co/mistralai/Mixtral-8x7B-v0.1)                                         |
| `alias-large-instruct`     | Instruction-fine-tuned version of the largest model, balancing accuracy with the ability to follow instructions effectively.              | [Mixtral 8x7B Instruct](https://huggingface.co/mistralai/Mixtral-8x7B-Instruct-v0.1)                         |

**OpenAI Model Aliases**

| Alias                      | Description                                                        | Current Model (May 2024)                                                                                 |
| -------------------------- | ------------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------------- |
| `gpt-3.5-turbo`            | OpenAI model used for the "turbo" mode, optimized for faster responses. | [Mistral](https://huggingface.co/mistralai/Mistral-7B-v0.1)                                                 |
| `gpt-4-turbo`              | OpenAI model used for the "turbo" mode, optimized for faster responses. | [Mixtral 8x7B](https://huggingface.co/mistralai/Mixtral-8x7B-v0.1)                                         |
| `text-embedding-ada-002`   | OpenAI model used for generating text embeddings.                     | [all-MiniLM-L6-v2](https://huggingface.co/sentence-transformers/all-MiniLM-L6-v2)                              |


**Development Models**

| Alias              | Description                                      |
| ------------------ | ------------------------------------------------ |
| [`dev-llama-3`](https://huggingface.co/blog/llama3)      | Development model for testing and experimentation. |
| [`dev-phi-3`](https://huggingface.co/microsoft/Phi-3-mini-4k-instruct)        | Development model for testing and experimentation. |
| [`dev-gemma`](https://huggingface.co/google/gemma-2-9b)        | Development model for testing and experimentation. Current Version: Gemma-2|
| [`dev-code-gemma`](https://huggingface.co/google/codegemma-7b)   | Development model for testing and experimentation. |
| [`dev-testgen`](https://huggingface.co/jacobhoffmann/CodeLLaMA-2-13B-TestGen-Dart_v0.2-GGUF)      | Development model for testing and experimentation. |

These models are not intended for production use and may be subject to change.
